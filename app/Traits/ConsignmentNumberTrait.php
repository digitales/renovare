<?php
namespace App\Traits;

use Carbon\Carbon;

trait ConsignmentNumberTrait
{

    /**
     * Generate the consignment number
     *
     * @param Carbon $date
     * @param null $prefex
     * @param null $suffix
     * @param int $length
     * @return string
     * @throws \Exception
     */
    public function generateConsignmentNumber(Carbon $date, $prefix = null, $suffix = null, $length = 5)
    {
        $randomString = $length > 0? bin2hex(random_bytes($length)) : '';
        // Super special algorithm
        return sprintf(
            '%1$s-%2$s-%3$s-%4$s',
            $prefix,
            $date->format('ymd'),
            $suffix,
            $randomString
        );
    }
}
