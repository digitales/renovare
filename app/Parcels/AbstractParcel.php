<?php
namespace App\Parcels;

use App\Couriers\CourierInterface;
use App\Exceptions\AttributeMissingException;
use App\Exceptions\CourierMissingException;
use Carbon\Carbon;

abstract class AbstractParcel
{

    public $created_at;

    /**
     *
     * @var integer in grams
     */
    protected $weight;

    /**
     * @var integer in pence
     */
    protected $value;

    /**
     * @var
     */
    protected $courier;


    protected $id;// Consignment number

    public function __construct(int $weight = 0, int $value = 0, $courier = null)
    {
        $this->setWeight($weight);
        $this->setValue($value);
        if ($courier) {
            $this->setCourier($courier);
        }
        $this->created_at = Carbon::now();
    }


    /**
     * @param string $name
     * @return mixed
     * @throws AttributeMissingException
     */
    public function __get(string $name)
    {
        if (method_exists(__CLASS__, 'get'.ucfirst($name))) {
            return $this->{'get'.ucfirst($name)}();
        }

        throw new AttributeMissingException($name.' attribute does not exist on '.__CLASS__);
    }

    /**
     * @param string $name
     * @param $value
     * @return mixed
     * @throws AttributeMissingException
     */
    public function __set(string $name, $value)
    {
        if (method_exists(__CLASS__, 'set'.ucfirst($name))) {
            return $this->{'set'.ucfirst($name)}($value);
        }
        throw new AttributeMissingException($name.' attribute does not exist on '.__CLASS__);
    }


    /**
     * ------------------------------------------------------------------------------------
     * Mutator methods
     * ------------------------------------------------------------------------------------
     * ------------------------------------------------------------------------------------
     */

    /**
     * Set the courier
     *
     * @param $courier
     * @return AbstractParcel fluent interface
     */
    public function setCourier($courier) : self
    {
        if (is_string($courier)) {
            $this->courier = new $courier;
            return $this;
        }
        $this->courier = $courier;
        return $this;
    }

    /**
     * Set the weight for the parcel
     *
     * @param int $weight
     * @return AbstractParcel
     */
    public function setWeight(int $weight) : self
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * Set the value of the parcel contents
     *
     * @param int $value
     * @return AbstractParcel
     */
    public function setValue(int $value) : self
    {
        $this->value = $value;
        return $this;
    }


    /**
     * ------------------------------------------------------------------------------------
     * Accessor methods
     * ------------------------------------------------------------------------------------
     * ------------------------------------------------------------------------------------
     */

    /**
     * Get the courier to be used for the parcel
     * @return mixed
     */
    public function getCourier()
    {
        return $this->courier;
    }

    /**
     * Get the weight of the parcel
     *
     * @return int
     */
    public function getWeight() : int
    {
        return $this->weight;
    }

    /**
     * Get the value of the parcel
     *
     * @return int
     */
    public function getValue() : int
    {
        return $this->value;
    }

    /**
     * Get the consignment ID for the parcel
     *
     * @return string
     * @throws CourierMissingException
     */
    public function getId() : string
    {
        if ($this->id) {
            return $this->id;
        }

        if (! $this->courier) {
            throw new CourierMissingException();
        }

        return $this->id = $this->courier->getConsignmentNumber($this);
    }


    /**
     * ------------------------------------------------------------------------------------
     * Other methods
     * ------------------------------------------------------------------------------------
     * ------------------------------------------------------------------------------------
     */

    /**
     * Regenerate the ID for the parcel
     *
     * @return string
     * @throws CourierMissingException
     */
    public function regenerateId() : string
    {
        $this->id = null;
        return $this->getId();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id.','.$this->weight.','.$this->value;
    }
}
