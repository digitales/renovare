<?php
namespace App\Couriers;

use App\Parcels\AbstractParcel;
use Illuminate\Support\Collection;

interface CourierInterface
{
    public function getConsignmentNumber(AbstractParcel $parcel);

    public function notify(Collection $parcelList);
}
