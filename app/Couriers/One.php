<?php
namespace App\Couriers;

use App\Parcels\AbstractParcel;
use App\Traits\ConsignmentNumberTrait;
use Illuminate\Support\Collection;

class One implements CourierInterface
{
    use ConsignmentNumberTrait;

    /**
     * Get the consignment number.
     *
     * @param AbstractParcel $parcel
     * @return string
     * @throws \Exception
     */
    public function getConsignmentNumber(AbstractParcel $parcel)
    {
        return $this->generateConsignmentNumber(
            $parcel->created_at,
            'ONE-',
            $parcel->weight.'-'.$parcel->value
        );
    }

    /**
     * Notify the courier of the parcels.
     *
     * @param Collection $parcelList
     * @return string
     */
    public function notify(Collection $parcelList)
    {
        return $parcelList->toJson();
    }
}
