<?php
namespace App\Couriers;

use App\Parcels\AbstractParcel;
use App\Traits\ConsignmentNumberTrait;
use Illuminate\Support\Collection;

class Two implements CourierInterface
{

    use ConsignmentNumberTrait;

    /**
     * Get the consignment number
     *
     * @param AbstractParcel $parcel
     * @return string
     * @throws \Exception
     */
    public function getConsignmentNumber(AbstractParcel $parcel)
    {
        return $this->generateConsignmentNumber(
            $parcel->created_at,
            'TWO-',
            $parcel->weight.'-'.$parcel->value
        );
    }

    /**
     * Notify the courier of the parcels
     *
     * @param Collection $parcelList
     * @return string
     */
    public function notify(Collection $parcelList)
    {
        // Return a comma separated list of parcel details
        return $parcelList->transform(function ($parcel) {
            return (string)$parcel;
        })->implode("\n\r");
    }
}
