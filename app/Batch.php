<?php
namespace App;

use App\Couriers\CourierInterface;
use App\Exceptions\ParcelNotFoundException;
use App\Parcels\AbstractParcel;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class Batch
{

    /**
     * @var Carbon Created at
     */
    public $created_at;

    /**
     * The list of parcels
     *
     * @var Collection|null
     */
    private $parcels;

    /**
     * Batch constructor.
     *
     * @param Collection|null $parcels
     */
    public function __construct(Collection $parcels = null)
    {
        $this->init($parcels);
        $this->created_at = Carbon::now();
    }

    /**
     * Initialise the batch
     *
     * @param Collection|null $parcels
     * @return void
     */
    public function init(Collection $parcels = null)
    {
        $this->parcels = !$parcels? new Collection($parcels) : $parcels;
    }

    /**
     * Start the batch
     *
     * @param void
     * @return void
     */
    public function start()
    {
        if ($this->parcels->count() == 0) {
            $this->init();
        }
    }

    /**
     * Add a parcel to this batch
     *
     * @param AbstractParcel $parcel
     * @return $this fluent interface
     */
    public function add(AbstractParcel $parcel)
    {
        $parcel->id; // Force the consignment ID to be generated.
        $this->parcels->push($parcel);
        return $this;
    }

    /**
     * Remove a parcel from the batch
     *
     * @param string $id
     * @return $this
     * @throws ParcelNotFoundException
     */
    public function remove(string $id)
    {
        $parcelToRemove = $this->getParcel($id);

        if ($parcelToRemove->count() == 0) {
            throw new ParcelNotFoundException();
        }

        $this->parcels->forget($parcelToRemove->keys()->first());
        return $this;
    }

    /**
     * Get a parcel from the batch
     *
     * @param string $id
     * @param bool $returnCollection
     * @return mixed
     */
    public function get(string $id, $returnCollection = false)
    {
        return $this->getParcel($id)->first();
    }



    /**
     * Change the courier for a given parcel ID.
     *
     * @param string $id
     * @param CourierInterface $courier
     * @return Collection
     */
    public function changeCourier(string $id, CourierInterface $courier)
    {
        return $this->parcels->transform(function ($parcel) use ($id, $courier) {
            if ($parcel->id == $id) {
                $parcel->setCourier($courier);
                $parcel->regenerateId();
            }
            return $parcel;
        });
    }


    /**
     * Get the list of parcels
     * @return Collection|null
     */
    public function getParcels()
    {
        return $this->parcels;
    }


    /**
     * End the batch and notify each of the couriers
     *
     * @return bool
     */
    public function end($debug = false)
    {
        // We need to get the list of parcels for each courier
        $parcelList = $this->groupByCourier($this->parcels);

        // Now we have split the lists of parcels, let's process the list for each courier.
        $parcelList->each(function ($parcels) use ($debug) {
            $result = $parcels->first()->courier->notify($parcels);
            if (true == $debug) {
                echo $result;
            }
        });

        return true;
    }

    /**
     * Group the list of parcels by courier
     *
     * @param Collection $parcelList
     * @return Collection
     */
    public function groupByCourier(Collection $parcelList)
    {
        // Or could map to groups
        return $parcelList->mapToGroups(function ($parcel) {
            return [get_class($parcel->courier) => $parcel];
        });
    }

    /**
     * Return a collection of the parcel from the batch
     *
     * @param string $id
     * @return Collection
     */
    protected function getParcel(string $id) : Collection
    {
        return $this->parcels->filter(function ($parcel) use ($id) {
            return $parcel->id === $id;
        });
    }
}
