<?php
namespace Tests\Unit;

use App\Couriers\One;
use App\Couriers\Two;
use App\Exceptions\AttributeMissingException;
use App\Exceptions\CourierMissingException;
use App\Parcels\Parcel;
use PHPUnit\Framework\TestCase;

class ParcelTest extends TestCase
{

    private $parcel;

    public function setUp()
    {
        $this->parcel = new Parcel(2, 3, One::class);
        parent::setUp();
    }


    /** @test */
    public function canCreateParcel()
    {
        $this->assertInstanceOf(Parcel::class, $this->parcel);
    }

    /** @test */
    public function canChangeCourier()
    {
        $this->assertInstanceOf(Parcel::class, $this->parcel);

        $this->assertInstanceOf(One::class, $this->parcel->courier);
        $this->assertInstanceOf(One::class, $this->parcel->getCourier());

        $this->parcel->setCourier(Two::class);
        $this->assertInstanceOf(Two::class, $this->parcel->getCourier());

        $this->parcel->setCourier(new One());
        $this->assertInstanceOf(One::class, $this->parcel->getCourier());
    }

    /** @test */
    public function canGetWeightOfParcel()
    {
        $parcel = new Parcel(200);

        $this->assertEquals(200, $parcel->weight);
        $this->assertEquals(200, $parcel->getWeight());
    }


    /** @test */
    public function canGetValueOfParcel()
    {
        $parcel = new Parcel(rand(1, 20), 20);

        $this->assertEquals(20, $parcel->value);
        $this->assertEquals(20, $parcel->getValue());
    }

    /** @test */
    public function canSetWeightOfParcel()
    {
        $parcel = new Parcel(200);

        $this->assertEquals(200, $parcel->weight);

        $parcel->weight = 22;

        $this->assertEquals(22, $parcel->weight);
    }


    /** @test */
    public function canSetValueOfParcel()
    {
        $parcel = new Parcel(rand(1, 20), 20);

        $this->assertEquals(20, $parcel->value);

        $parcel->value = 300;

        $this->assertEquals(300, $parcel->value);
    }


    /** @test */
    public function canNotGetNonExistentAttributes()
    {
        $this->expectException(AttributeMissingException::class);
        $this->parcel->NonExistantAttribute;
    }

    /** @test */
    public function canNotSetNonExistentAttributes()
    {
        $this->expectException(AttributeMissingException::class);
        $this->parcel->NonExistantAttribute = 'error';
    }

    /** @test */
    public function canGetConsignmentId()
    {
        $this->assertNotNull($this->parcel->id);
        $this->assertNotNull($this->parcel->id);
    }

    /** @test */
    public function expectErrorWhenCourierIsMissing()
    {
        $parcel = new Parcel(rand(1, 30), rand(1, 200));

        $this->expectException(CourierMissingException::class);
        $parcel->getId();
    }

    /** @test */
    public function canOutputParcelAsString()
    {
        $this->assertNotNull((string)$this->parcel);
    }
    /** @test */
    public function canRegenerateConsignmentId()
    {
        $id = $this->parcel->id;

        $this->assertNotEquals($id, $this->parcel->regenerateId());
    }
}
