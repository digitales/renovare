<?php
namespace Tests\Unit;

use App\Batch;
use App\Couriers\One;
use App\Couriers\Two;
use App\Exceptions\CourierMissingException;
use App\Exceptions\ParcelNotFoundException;
use App\Parcels\AbstractParcel;
use App\Parcels\Parcel;
use Illuminate\Support\Collection;

class BatchTest extends \PHPUnit\Framework\TestCase
{

    protected $batch;

    public function setUp()
    {
        $this->batch = new Batch();
        parent::setUp();
    }

    /** @test */
    public function canCreateAndStartABatch()
    {
        $batch = new Batch();
        $this->assertNotNull($batch->created_at);

        $batch->start();
        $this->assertNotNull($batch->created_at);
    }

    /** @test */
    public function canAddAParcelToBatch()
    {
        $this->assertEquals(0, $this->batch->getParcels()->count());

        $parcel = new Parcel(20, 40, new One());
        $this->batch->add($parcel);

        $this->assertEquals(1, $this->batch->getParcels()->count());
        $this->assertInstanceOf(AbstractParcel::class, $this->batch->get($parcel->id));
    }

    /** @test */
    public function canNoAddParcelWithNoCourier()
    {
        $parcel = new Parcel(20, 40);

        $this->assertEquals(0, $this->batch->getParcels()->count());

        $this->expectException(CourierMissingException::class);

        $this->batch->add($parcel);
    }


    /** @test */
    public function canGetListOfParcelsFromBatch()
    {
        $parcels = $this->mockParcels(6);

        $this->assertEquals(0, $this->batch->getParcels()->count());

        foreach ($parcels as $parcel) {
            $this->batch->add($parcel);
        }

        $this->assertEquals(6, $this->batch->getParcels()->count());
    }

    /** @test */
    public function canRemoveParcelFromBatch()
    {
        $parcels = $this->mockParcels(6);

        $this->assertEquals(0, $this->batch->getParcels()->count());

        foreach ($parcels as $parcel) {
            $this->batch->add($parcel);
        }

        $this->assertEquals(6, $this->batch->getParcels()->count());

        $id = $parcels[3]->id;

        $this->batch->remove($id);

        $this->assertEquals(5, $this->batch->getParcels()->count());
    }

    /** @test */
    public function canOnlyRemoveFromBatchOnce()
    {
        $parcels = $this->mockParcels(6);

        $this->assertEquals(0, $this->batch->getParcels()->count());

        foreach ($parcels as $parcel) {
            $this->batch->add($parcel);
        }

        $this->assertEquals(6, $this->batch->getParcels()->count());

        $id = $parcels[3]->id;

        $this->batch->remove($id);

        $this>$this->expectException(ParcelNotFoundException::class);

        $this->batch->remove($id);
    }


    /** @test */
    public function canChangeCourierForParcel()
    {
        $parcel = $this->mockParcels(1, One::class)[0];
        $this->batch->add($parcel);

        $this->assertInstanceOf(One::class, $this->batch->get($parcel->id)->courier);

        $this->batch->changeCourier($parcel->id, new Two());

        $this->assertInstanceOf(Two::class, $this->batch->get($parcel->id)->courier);
    }

    /** @test */
    public function canGroupParcelsByCourier()
    {
        $courierOneParcels = $this->mockParcels(4, One::class);
        $courierTwoParcels = $this->mockParcels(6, Two::class);

        $this->assertEquals(4, count($courierOneParcels));
        $this->assertEquals(6, count($courierTwoParcels));

        foreach ($courierOneParcels as $parcel) {
            $this->batch->add($parcel);
        }

        foreach ($courierTwoParcels as $parcel) {
            $this->batch->add($parcel);
        }
        $this->assertEquals(10, $this->batch->getParcels()->count());

        $grouped = $this->batch->groupByCourier($this->batch->getParcels());

        $this->assertEquals(4, $grouped[One::class]->count());
        $this->assertEquals(6, $grouped[Two::class]->count());
    }

    /** @test */
    public function canEndBatch()
    {
        $this->assertTrue($this->batch->end());
    }


    protected function mockParcels($numberOfParcels = 1, $courier = One::class)
    {
        $return = [];

        for ($i= 0; $i < $numberOfParcels; $i++) {
            $return[$i]= new Parcel(rand(1, 20), rand(1, 1000), $courier);
        }
        return $return;
    }
}
