<?php

namespace Tests\Unit\Traits;

use App\Traits\ConsignmentNumberTrait;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class ConsignmentNumberTraitTest extends TestCase
{
    public $trait;

    public function setUp()
    {
        $this->trait = $this->getMockForTrait(ConsignmentNumberTrait::class);
        parent::setUp();
    }

    /**
     * @test
     */
    public function canGenerateConsignmentNumber()
    {
        $date = Carbon::create('2017', '12', '24');
        $this->assertEquals('ONE-171224-TEST-', $this->trait->generateConsignmentNumber($date, 'ONE', 'TEST', 0));
    }
}
