# Renovare Technical Test

Author: Ross Tweedie <ross@nthdesigns.co.uk>

## Description
This challenge is designed to test your PHP7 design and implementation skills. We will pay particular attention to
choices of design patterns and coding standards as well as reviewing your object structure, testing, commenting
and overall understanding of the language.

## Scenario
We have a client, Ben, who sells duvets online and instore. Ben needs an order dispatch system to send out the
orders with a number of couriers.

At the start of a day, an order batch can be started. During the day parcels can be added to the batch. Parcels
added require a weight, the value of the items and will be given a unique consignment number based on an
algorithm provided by the courier.

At the end of the day the batch can be closed and a list of all of the parcels added during the day will need to
be sent to the various couriers. Each courier will have different requirements for receiving the data. Some will
require an email of the list, others may require a JSON post to a given end point etc.

The batch duration could change (weekends may only have 1 batch period) and will be open and closed
manually by the client.

## What you should create
Build a class library to satisfy the above scenario. Keep in mind that at some point the library will be handed to
another developer to create an API or HTML interface for the client.
Keep in mind the following functions:
* Starting a new batch
* Adding parcels to the batch
* Removing parcels from the batch (cancelled orders)
* Changing courier of a parcel (mistakes happen)
* Closing the batch

## Installation
1. Clone the repository.
2. Navigate to the cloned files
3. Run `composer update`
4. Run `php client.php`

## Requirements
To run the code PHP 7.xx is required.

Tested up to PHP 7.2.5 CLI
