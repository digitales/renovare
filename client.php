<?php
/**
 *
 * Batch client
 *
 */
require __DIR__.'/vendor/autoload.php';


// Create and start new batch
$batch = new \App\Batch();
$batch->start();

// Add package
$packageOne = new \App\Parcels\Parcel(200, 500);
$packageOne->setCourier(new \App\Couriers\One());

$batch->add($packageOne);

$package2 = new \App\Parcels\Letter(rand(1, 100), rand(1, 1000), \App\Couriers\Two::class);
$batch->add($package2);

$package3 = new \App\Parcels\Letter(rand(1, 100), rand(1, 1000), \App\Couriers\One::class);
$batch->add($package3);

$package4 = new \App\Parcels\Letter(rand(1, 100), rand(1, 1000), \App\Couriers\Two::class);
$batch->add($package4);

$package5 = new \App\Parcels\Letter(rand(1, 100), rand(1, 1000), \App\Couriers\Two::class);
$batch->add($package5);

$package6 = new \App\Parcels\Letter(rand(1, 100), rand(1, 1000), \App\Couriers\Two::class);
$batch->add($package6);


// Remove package
$removeParcel = $batch->remove($package5->id);

$package7 = new \App\Parcels\Letter(rand(1, 100), rand(1, 1000), \App\Couriers\Two::class);
$batch->add($package7);

// Change courier
// Change the Courier
$courierOne = new \App\Couriers\One();
$batch->changeCourier($package3->id, $courierOne);
// We need to regenerate the consignment ID.


// Close batch
$batch->end(true);
